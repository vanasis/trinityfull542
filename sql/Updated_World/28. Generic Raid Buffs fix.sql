DELETE FROM `spell_script_names` WHERE `spell_id` IN(1126, 1459, 19740, 20217, 21562, 61316, 109773, 116781);
INSERT INTO `spell_script_names` (`spell_id`, `ScriptName`) VALUES (1126, 'spell_gen_raid_buff'); -- Mark of the Wild
INSERT INTO `spell_script_names` (`spell_id`, `ScriptName`) VALUES (1459, 'spell_gen_raid_buff'); -- Arcane Brillianc
INSERT INTO `spell_script_names` (`spell_id`, `ScriptName`) VALUES (19740, 'spell_gen_raid_buff'); -- Blessing of Might
INSERT INTO `spell_script_names` (`spell_id`, `ScriptName`) VALUES (20217, 'spell_gen_raid_buff'); -- Blessing of Kings
INSERT INTO `spell_script_names` (`spell_id`, `ScriptName`) VALUES (21562, 'spell_gen_raid_buff'); -- Power Word: Fortitude
INSERT INTO `spell_script_names` (`spell_id`, `ScriptName`) VALUES (61316, 'spell_gen_raid_buff'); -- Dalaran Brilliance
INSERT INTO `spell_script_names` (`spell_id`, `ScriptName`) VALUES (109773, 'spell_gen_raid_buff'); -- Dark Intent
INSERT INTO `spell_script_names` (`spell_id`, `ScriptName`) VALUES (116781, 'spell_gen_raid_buff'); -- Legacy of the White Tiger
